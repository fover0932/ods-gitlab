#! /bin/sh

# Check time since the latest gitlab backup, if it is too long, then backup fuction failed, email to the administrator.
# Example:
# backup_time is 1:00 every day
# run this script when 1:30 every day 

# define the max diff time from building buckup to running the script 
TIME_DIFF_MAX=1800

# browser the backup dir to find the time of latest backup finished.
#time_when_build_latest_backup=`sudo -u root -H ls -l /srv/docker/gitlab/gitlab/backups/ |sed -n '8p'|awk '{print $9}'|cut -f 1 -d '_'`
time_when_build_latest_backup=`sudo -u root -H ls -lrt /srv/docker/gitlab/gitlab/backups/ | tail -1 |sed -n '1p'|awk '{print $9}'|cut -f 1 -d '_'`
# get the current time
time_now=`date +%s`
# get the diff time
time_diff=$time_now-$time_when_build_latest_backup


if [ $(($time_diff)) -gt $TIME_DIFF_MAX ];  then
    echo "gitlab build backup failed!" > gitlab_backup_mail.txt 
    echo "time now: $time_now, same as `date`;" >> gitlab_backup_mail.txt
    echo "time_latest_buckup: $time_when_build_latest_backup, same as `date --date="@$time_when_build_latest_backup"`;" >> gitlab_backup_mail.txt
    echo "time_diff is: $(($time_diff)), greater than $TIME_DIFF_MAX;" >> gitlab_backup_mail.txt
    mail -s "Gitlab backup build failed" 495456788@qq.com < gitlab_backup_mail.txt 
else 
    echo "gitlab build backup successfully on `date --date="@$time_when_build_latest_backup"`, the timestamp is $time_when_build_latest_backup" | mail -s "Gitlab backup build successfully" 495456788@qq.com
fi
