#! /bin/sh
# check disk avaliable use size, to stop website down before too late  

# define a alart limit
DISK_ALART_LIMIT=80

# get the disk avalable size
docker_disk_use=`df -h |sed -n '9p'|awk '{print $5}'|cut -f 1 -d '%'`

if [ $docker_disk_use -gt $DISK_ALART_LIMIT ];  then
    echo `date`" docker disk use $docker_disk_use%, over $DISK_ALART_LIMIT%." | mail -s "aliyun docker disk alart" 495456788@qq.com 
else
    echo `date`" docker disk use $docker_disk_use%, far from  $DISK_ALART_LIMIT%." | mail -s "aliyun docker disk well" 495456788@qq.com
fi
